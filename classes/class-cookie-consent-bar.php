<?php
/**
 * Cookie consent bar
 *
 * @package Gital Cookie Consent
 */

namespace g_cookie_consent;

if ( ! class_exists( 'Cookie_Consent_Bar' ) ) {
	/**
	 * Cookie_Consent_Bar
	 *
	 * Defines and renders the bar
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.3.0
	 * @since 1.5.0
	 */
	class Cookie_Consent_Bar {

		public function __construct() {
			add_action( 'wp_footer', array( $this, 'the_bar' ) );
		}

		/**
		 * The bar
		 *
		 * Renders the cookie consent bar
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function the_bar() {
			echo $this->get_bar();
		}

		/**
		 * Get bar
		 *
		 * Returns the cookie consent bar
		 *
		 * @return String The bar in HTML
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.2.1
		 * @since 1.5.0
		 */
		private function get_bar() {
			$options = get_option( 'g_cc_settings' );

			// Render the bar.
			if ( is_array( $options ) && $options['text'] && $options['more-info-text'] && $options['more-info-link'] && $options['title'] && $options['accept-text'] ) {
				$cookie_consent_bar = '<div id="g-cookie-consent" class="g-cookie-consent g-cookie-consent--no-process"><div class="g-cookie-consent__wrapper">';

				if ( ! get_permalink( $options['more-info-link'] ) ) {
					$options['more-info-link'] = esc_url( $options['more-info-link'] );
				} else {
					$options['more-info-link'] = get_permalink( $options['more-info-link'] );
				}

				$cookie_consent_bar_button_class = apply_filters( 'g_cc_bar_button_class', 'g-button--light' );

				$cookie_consent_bar .= '<div class="g-cookie-consent__text"><h4>' . esc_html( $options['title'] ) . '</h4><p>' . esc_html( $options['text'] ) . ' <a href="' . $options['more-info-link'] . '" target="_blank" class="g-cookie-consent__text__more-info">' . esc_html( $options['more-info-text'] ) . '</a></p></div>';
				$cookie_consent_bar .= '<div class="g-cookie-consent__button"><button id="g-consent" class="g-button g-button--small ' . esc_attr( $cookie_consent_bar_button_class ) . '">' . esc_html( $options['accept-text'] ) . '</button></div>';
				$cookie_consent_bar .= '</div></div>';
			}

			// Render the bar.
			if ( isset( $cookie_consent_bar ) ) {
				return $cookie_consent_bar;
			} else {
				return '';
			}
		}
	}
}
