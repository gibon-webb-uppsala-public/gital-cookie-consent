<?php
/**
 * Cookie consent settings
 *
 * @package Gital Cookie Consent
 */

namespace g_cookie_consent;

if ( ! class_exists( 'Cookie_Consent_Settings' ) ) {
	/**
	 * Cookie_Consent_Settings
	 *
	 * Sets up the cookie consent settings, settings page and applies a test cookie
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.3
	 * @since 1.5.0
	 */
	class Cookie_Consent_Settings {

		public function __construct() {
			add_action( 'wp', array( $this, 'add_test_cookie' ) );
			add_action( 'admin_menu', array( $this, 'settings_option_page' ) );
			add_action( 'admin_init', array( $this, 'settings_init' ) );
		}

		/**
		 * Add test cookie
		 *
		 * Add test cookie
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.5.0
		 */
		public function add_test_cookie() {
			if ( version_compare( phpversion(), '7.3.0', '>' ) ) {
				setcookie(
					'g_cookie_consent_test',
					1,
					array(
						'expires'  => time() + 3600,
						'path'     => COOKIEPATH,
						'domain'   => COOKIE_DOMAIN,
						'samesite' => 'Strict',
					)
				);
			} else {
				setcookie(
					'g_cookie_consent_test',
					1,
					time() + 3600,
					COOKIEPATH,
					COOKIE_DOMAIN
				);
			}
		}

		/**
		 * Settings option page
		 *
		 * Add settings page
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 * @since 1.5.0
		 */
		public function settings_option_page() {
			add_options_page( __( 'Cookie Consent', 'gital-cookie-consent' ), __( 'Cookie Consent', 'gital-cookie-consent' ), 'administrator', 'g-cc-settings', array( $this, 'settings' ) );
		}

		/**
		 * Settings init
		 *
		 * Add content to settings page
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.2
		 * @since 1.5.0
		 */
		public function settings_init() {
			// Define the settings.
			register_setting( 'g_cc', 'g_cc_settings' );

			// Define the section.
			add_settings_section(
				'settings_section',
				__( 'Settings', 'gital-cookie-consent' ),
				array( $this, 'settings_section_description' ),
				'g_cc'
			);

			// Define the fields.
			add_settings_field(
				'title',
				__( 'Title', 'gital-cookie-consent' ),
				array( $this, 'settings_title_render' ),
				'g_cc',
				'settings_section'
			);

			add_settings_field(
				'text',
				__( 'Text', 'gital-cookie-consent' ),
				array( $this, 'settings_text_render' ),
				'g_cc',
				'settings_section'
			);

			add_settings_field(
				'more-info-text',
				__( 'More info text', 'gital-cookie-consent' ),
				array( $this, 'settings_more_info_text_render' ),
				'g_cc',
				'settings_section'
			);

			add_settings_field(
				'more-info-link',
				__( 'More info link', 'gital-cookie-consent' ),
				array( $this, 'settings_more_info_link_render' ),
				'g_cc',
				'settings_section'
			);

			add_settings_field(
				'accept-text',
				__( 'Accept text', 'gital-cookie-consent' ),
				array( $this, 'settings_accept_text_render' ),
				'g_cc',
				'settings_section'
			);
		}

		/**
		 * Settings section description()
		 *
		 * Renders the description to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_section_description() {
			echo '<p>' . esc_html__( 'Enter the settings for the Cookie Consent bar.', 'gital-cookie-consent' ) . '</p>';
		}

		/**
		 * Settings title render
		 *
		 * Renders the title to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_title_render() {
			$options = get_option( 'g_cc_settings' );
			echo '<input type="text" name="g_cc_settings[title]" value="' . esc_html( $options['title'] ) . '">';
		}

		/**
		 * Settings text render
		 *
		 * Renders the text to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_text_render() {
			$options = get_option( 'g_cc_settings' );
			echo '<textarea name="g_cc_settings[text]" rows="3" cols="80">' . esc_html( $options['text'] ) . '</textarea>';
		}

		/**
		 * Settings more info text render
		 *
		 * Renders the more info text to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_more_info_text_render() {
			$options = get_option( 'g_cc_settings' );
			echo '<input type="text" name="g_cc_settings[more-info-text]" value="' . esc_html( $options['more-info-text'] ) . '">';
		}

		/**
		 * Settings more info link render
		 *
		 * Renders the more info link to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_more_info_link_render() {
			$options = get_option( 'g_cc_settings' );
			echo '<input type="text" name="g_cc_settings[more-info-link]" value="' . esc_html( $options['more-info-link'] ) . '">';
			echo '<p>' . esc_html__( 'You can add a page ID as well as an URL.', 'gital-cookie-consent' ) . '</p>';
		}

		/**
		 * Settings accept text render
		 *
		 * Renders the accept text to the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings_accept_text_render() {
			$options = get_option( 'g_cc_settings' );
			echo '<input type="text" name="g_cc_settings[accept-text]" value="' . esc_html( $options['accept-text'] ) . '">';
		}

		/**
		 * Settings
		 *
		 * Renders the form
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function settings() {
			echo '<form action="options.php" method="post">';
			echo '<h1>' . esc_html__( 'Cookie Consent', 'gital-cookie-consent' ) . '</h1>';
			settings_fields( 'g_cc' );
			do_settings_sections( 'g_cc' );
			submit_button();
			echo '</form>';
		}
	}
}
