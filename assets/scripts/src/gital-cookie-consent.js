// Set cookie
function gSetCookie(cname, cvalue, exdays, samesite) {
	var d = new Date();
	d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
	var expires = 'expires=' + d.toUTCString();
	document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/;samesite=' + samesite;
}
window.gSetCookie = gSetCookie;

// Get cookie
function gGetCookie(cname) {
	var name = cname + '=';
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return '';
}
window.gGetCookie = gGetCookie;

jQuery(document).ready(function ($) {
	// Set cookie when consented function
	$('#g-consent').on('click', function () {
		var gConsentBar = $('#g-cookie-consent');
		console.info('Cookies Consented');
		gConsentBar.addClass('g-cookie-consent--consented');
		gConsentBar.removeClass('g-cookie-consent--not-consented');
		gSetCookie('g_cookie_consent', 1, 7, 'strict');
		setTimeout(function () {
			gConsentBar.hide();
		}, 3000);
	});

	// Control status
	if (gGetCookie('g_cookie_consent')) {
		var cookieStatus = 1; // Has consented
	} else {
		if (!gGetCookie('g_cookie_consent_test')) {
			gSetCookie('g_cookie_consent_test', 1, 7, 'strict');
		}
		if (gGetCookie('g_cookie_consent_test')) {
			var cookieStatus = 0; // Has not consented
		} else {
			var cookieStatus = 2; // Does not accept cookies
		}
	}

	// Toggle status
	var gConsentBar = $('#g-cookie-consent');
	if (gConsentBar.length === 1) {
		if (cookieStatus == 0) {
			gConsentBar.addClass('g-cookie-consent--not-consented');
			gConsentBar.removeClass('g-cookie-consent--no-process');
		} else if (cookieStatus == 2) {
			gConsentBar.addClass('g-cookie-consent--no-cookies');
			gConsentBar.removeClass('g-cookie-consent--no-process');
			gConsentBar.hide();
		}
	}
});
