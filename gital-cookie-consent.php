<?php
/**
 * Plugin Name: Gital Cookie Consent
 * Author: Gibon Webb Uppsala
 * Version: 1.10.0
 * Text Domain: gital-cookie-consent
 * Domain Path: /languages/
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Cookie Consent is made with passion in Uppsala, Sweden. The plugin is adding a cookie consent disclaimer to the page. If you'd like support, please contact us at webb.uppsala@gibon.se.
 *
 * @package Gital Cookie Consent
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

// Init the updater
// Source: https://github.com/YahnisElsts/plugin-update-checker.
require plugin_dir_path( __FILE__ ) . 'vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-cookie-consent.json',
	__FILE__,
	'gital-cookie-consent'
);

/**
 * Load textdomain
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.5.0
 */
function g_cc_load_textdomain() {
	load_plugin_textdomain( 'gital-cookie-consent', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'g_cc_load_textdomain', 10 );

/**
 * Resources
 *
 * Enquene public style and scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 1.5.0
 */
function g_cc_resources() {
	wp_enqueue_style(
		'gital_cookie-consent_style',
		plugins_url( 'assets/styles/gital.cookie.min.css', __FILE__ ),
		false,
		'1.1',
		'all'
	);
	wp_register_script(
		'gital_cookie-consent_script',
		plugins_url( 'assets/scripts/gital.cookie.min.js', __FILE__ ),
		array( 'jquery' ),
		'1.0',
		true
	);
	wp_enqueue_script( 'gital_cookie-consent_script' );
}
add_action( 'wp_enqueue_scripts', 'g_cc_resources' );

// Constants.
if ( ! defined( 'G_CC_ROOT_PATH' ) ) {
	define( 'G_CC_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_CC_CLASSES_PATH' ) ) {
	define( 'G_CC_CLASSES_PATH', G_CC_ROOT_PATH . 'classes/' );
}

// Classes.
require_once G_CC_CLASSES_PATH . 'class-cookie-consent-bar.php';
require_once G_CC_CLASSES_PATH . 'class-cookie-consent-settings.php';

// Init the cookie consent settings.
new g_cookie_consent\Cookie_Consent_Settings();

// Only show the bar if not in the admin area.
if ( ! is_admin() ) {
	// Return if already consented.
	if ( isset( $_COOKIE['g_cookie_consent'] ) ) {
		return;
	} else {
		new g_cookie_consent\Cookie_Consent_Bar();
	}
}
