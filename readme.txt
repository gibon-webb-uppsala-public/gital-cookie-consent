=== Gital Cookie Consent ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Cookie Consent is made with passion in Uppsala, Sweden. The plugin is adding a cookie consent disclaimer to the page. If you'd like support, please contact us at webb.uppsala@gibon.se.

== Description ==
The Gital Cookie Consent is made with passion in Uppsala, Sweden. The plugin is adding a cookie consent disclaimer to the page. If you'd like support, please contact us at webb.uppsala@gibon.se.

== Installation ==
Add the plugin and edit the settings from the settings menu.

== Changelog ==

= 1.10.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 1.9.1 - 2022.06.09 =
* Bugfix: Removed the getCookie and setCookie from the window namespace in JS. 

= 1.9.0 - 2022.01.10 =
* Update: The policy link text is now an inline-block without margin and the bar text is now aligned to the left. 

= 1.8.2 - 2021.11.12 =
* Bugfix: Changed "init" to "wp" in the action hook for the test cookie. 

= 1.8.1 - 2021.08.19 =
* Update: Added filter 'g_cc_bar_button_class' to be able to change the class of the button.

= 1.7.5 - 2021.08.19 =
* Bugfix: get_bar got updated with a small bugfix.

= 1.7.4 =
* Update: Changed 100vw to 100% to make it look better in Firefox.

= 1.7.1 =
* Update: Updated all npm packages.
* Update: Updated to math.div in Sass instead of /.

= 1.7.0 =
* Update: Added WPML String translation support

= 1.6.2 =
* Update: setcookie is now working for PHP-versions below 7.3

= 1.6.1 =
* Update: Addex box-sizing to the consent bar

= 1.6.0 =
* Update: Updated the documentation and cleaned up the plugin

= 1.5.4 =
* Update: The text field is now an textarea instead of a single line

= 1.5.3 =
* Bugfix: Function settings_init had the wrong value in register_setting

= 1.5.2 =
* Bugfix: Error on settings page

= 1.5.0 =
* Update: Cleaned up the plugin and moved over to classes

= 1.4.3 =
* Update: Added css variables fall for older browsers

= 1.4.2 =
* Update: Moved plugin-update-checker to composer

= 1.3.2 =
* Bugfix: Not demanding gital external script anymore

= 1.3.1 =
* Bugfix: setcookie() update.

= 1.3.0 =
* The bar won't get rendered if the visitor already has consented.
* The function to add cookies now always use "Strict" SameSite value.

= 1.2.0 =
* Added posibility to add a custom URL

= 1.1.12 =
* Added fallback to css-variables
= 1.1.12 =
* Added fallback to css-variables

= 1.1.11 =
* Update to fix .git error

= 1.1.8 =
* composer test

= 1.1.7 =
* Small bugfix to composer support

= 1.1.6 =
* Updated composer support

= 1.1.5 =
* Test

= 1.1.4 =
* Added composer support

= 1.1.3 =
* Corrected the link styling

= 1.1.2 =
* Bugfixes

= 1.1.1 =
* Updated language files

= 1.1.0 =
* Added updater and moved to wp settings

= 1.0.3 =
* Added SameSite=Strict to cookies
* Moved the setting of the cookie to javascript instead of php

= 1.0.2 =
* Added internationalization

= 1.0.1 =
* Fixed bug that caused the bar to show directly if the content is smaller than the window

= 1.0.0 =
* Builded