��    
      l       �       �      �      �   .   �                %     .     3  (   9  �  b     �       7        M     \     j     y     ~  �   �   Accept text Cookie Consent Enter the settings for the Cookie Consent bar. More info link More info text Settings Text Title You can add a page ID as well as an URL. Project-Id-Version: Gital Cookie Consent
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-19 13:29+0000
PO-Revision-Date: 2021-02-11 08:55+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.4; wp-5.5.3 Acceptanstext Cookie Samtycke Lägg in inställningarna för cookie samtyckes baren.  Mer info länk Mer info text Inställningar Text Titel Du kan ange ett ID till en sida på denna site eller en adress till en annan sida. Det går även bra att ange en permalänk, exempelvis: /cookie-policy 