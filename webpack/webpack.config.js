/*
 * WEBPACK CONFIG
 *
 * Author : Gustav Gesar
 * Version: 2.0.0
 *
 */

// Plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const path = require('path');

const config = {
	entry: {
		// Gital Cookie Consent
		// Styles
		'../assets/styles/gital.cookie': './assets/styles/src/gital-cookie-consent.scss',

		// Scripts
		'../assets/scripts/gital.cookie': './assets/scripts/src/gital-cookie-consent.js',
	},

	output: {
		path: path.resolve(__dirname),
		filename: '[name].min.js',
		sourceMapFilename: '[file].map',
		chunkLoading: false,
		wasmLoading: false,
	},

	stats: 'minimal',

	module: {
		rules: [
			{
				// Scripts
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
					},
				},
			},
			{
				// Styles
				test: /\.(sa|sc|c)ss$/,

				// Loaders are applying from right to left
				use: [
					{
						// After all CSS loaders we use plugin to do his work.
						// It gets all transformed CSS and extracts it into separate
						// single bundled file
						loader: MiniCssExtractPlugin.loader,
					},
					{
						// This loader resolves url() and @imports inside CSS
						loader: 'css-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						// Then we apply postCSS fixes like autoprefixer and minifying
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							postcssOptions: {
								plugins: [
									require('autoprefixer')({
										grid: false,
										flexbox: false,
									}),
								],
							},
						},
					},
					{
						// First we transform SASS to standard CSS
						loader: 'sass-loader',
						options: {
							implementation: require('sass'),
							sourceMap: true,
						},
					},
				],
			},
			{
				// Images
				test: /\.(png|jpg)$/,
				loader: 'file-loader',
				options: {
					name: '[path][name].[ext]',
					emitFile: false,
					publicPath: function (url) {
						return url.replace('web', ''); // Removes "web" from the path when the public URLs are written
					},
				},
			},
			{
				// Vectors
				test: /\.(svg)$/,
				loader: 'url-loader',
			},
		],
	},
	plugins: [
		new RemoveEmptyScriptsPlugin(),
		new MiniCssExtractPlugin({
			filename: '[name].min.css',
		}),
		new BrowserSyncPlugin({
			host: 'localhost',
			port: 3000,
			proxy: process.env.WP_HOME,
		}),
	],
};

module.exports = config;
